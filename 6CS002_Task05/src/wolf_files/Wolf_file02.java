package wolf_files;
import java.io.*;

/******************************************************************************
 * This program counts the number of lines in a text file.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Wolf_file02 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
      new BufferedReader(new FileReader("src/wolf_files/wolf-fox.txt"));

    System.out.println(r.lines().count());

    r.close();
  }

}
