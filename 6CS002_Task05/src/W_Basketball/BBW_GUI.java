package W_Basketball;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class BBW_GUI extends JFrame {
private JPanel contentPane;
	
	public BBW_GUI() {
		setResizable(false);
		setTitle("6CS002_Task05_2049342");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 600);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel title = new JLabel("<html><h1><strong><i>WOMEN BASKETBALL</i></strong></h1><hr></html>");
		title.setBounds(210, 34, 45, 16);
		title.resize(300, 50);
		contentPane.add(title);
		
		JButton basktbl01 = new JButton("WOMEN BASKETBALL 01");
		basktbl01.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				W_Basketball_01.main(null);
			}
		});
		basktbl01.setBounds(200, 80, 117, 29);
		basktbl01.resize(200, 50);
		contentPane.add(basktbl01);
		
		JButton basktbl02 = new JButton("WOMEN BASKETBALL 02");
		basktbl02.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				W_Basketball_02.main(null);
			}
		});
		basktbl02.setBounds(200, 150, 117, 29);
		basktbl02.resize(200, 50);
		contentPane.add(basktbl02);
		
		JButton basktbl03 = new JButton("WOMEN BASKETBALL 03");
		basktbl03.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				W_Basketball_03.main(null);
			}
		});
		basktbl03.setBounds(200, 220, 117, 29);
		basktbl03.resize(200, 50);
		contentPane.add(basktbl03);
		
		JButton basktbl04 = new JButton("WOMEN BASKETBALL 04");
		basktbl04.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				W_Basketball_04.main(null);
			}
		});
		basktbl04.setBounds(200, 290, 117, 29);
		basktbl04.resize(200, 50);
		contentPane.add(basktbl04);
		
		JButton basktbl05 = new JButton("WOMEN BASKETBALL 05");
		basktbl05.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				W_Basketball_05.main(null);
			}
		});
		basktbl05.setBounds(200, 360, 117, 29);
		basktbl05.resize(200, 50);
		contentPane.add(basktbl05);
		
		JButton exit = new JButton("exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit.setBounds(200, 440, 117, 29);
		exit.resize(200, 50);
		contentPane.add(exit);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BBW_GUI frame = new BBW_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
