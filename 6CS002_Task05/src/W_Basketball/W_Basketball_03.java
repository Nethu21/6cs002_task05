package W_Basketball;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class W_Basketball_03 {
  public static void main(String[] args) {
	  List<WBBClub> table = Arrays.asList(
		        new WBBClub(1, "Southside Lynx", 11, 10, 0, 1, 493,0,450,43,6, 24,10,5),
		        new WBBClub(2, "Perth Flyers", 11, 6, 2, 3, 481,2,270,129,0,9,5,4),
		        new WBBClub(3, "Sydney Spirit", 11, 5, 1, 5, 480,2,225,215,0,0,6,3),
		        new WBBClub(4, "Flames Bendigo", 11, 4, 1, 6, 478,2,180,258,0,0,2,1),
		        new WBBClub(5, "Townsville Flames", 11, 7,2,2,485,0,315,86,3,3,3,2),
		        new WBBClub(6, "Coburg Kougars", 11, 8, 1, 2, 488,0,360,86,3,6,4,2),
		        new WBBClub(7, "St. Kilda", 11, 5, 1, 5, 480,2,225,215,0,0,2,1),
		        new WBBClub(8, "Melbourne Sharks", 11, 8, 1, 2, 488,0,360,86,3,9,5,4),
		        new WBBClub(9, "Sutherland Tigers", 11, 5, 2, 24, 481,0,225,172,0,0,2,1),
		        new WBBClub(10, "East Bearcats", 11, 9, 1, 1, 488,2,405,43,3,0,2,1),
		        new WBBClub(11, "City Comets", 11, 6, 1, 4, 484, 0,270,172,0,3,1,1),
		        new WBBClub(12, "SLSM Hawks", 11, 11, 0, 0, 495,0,495,0,9,44,14,7));
		       

    OptionalInt bb_min = table.stream().mapToInt(WBBClub::getpointscored).min();
    if (bb_min.isPresent()) {
      System.out.printf("Lowest number of points scored by a team %d\n", bb_min.getAsInt());
    } else {
      System.out.println("min failed");
    }
    
    OptionalInt bb_max = table.stream().mapToInt(WBBClub::getpointscored).max();
    if (bb_max.isPresent()) {
    	System.out.printf("Highest number of points scored by a team : %d\n", bb_max.getAsInt());
    } else {
    	System.out.println("max failed");
    }
    
    // reduce
    System.out.println("Total matches won:");
    Integer bb_output = table.stream().map(WBBClub::getWon).reduce(0, (a, b) -> a + b);
    System.out.println(bb_output);
    
    //Collectors
    List<String> bb_result = table.stream().filter(p -> p.getthreePointersMade() > 8).map(WBBClub::getClub)
    		.collect(Collectors.toList());
    System.out.println(bb_result.toString());
  
    try {
        FileWriter writer = new FileWriter("WBB_OUTPUT3.txt");
        writer.write("Lowest number of points scored by a team:" + bb_min.getAsInt() + "\n");
        writer.write("Highest number of points scored by a team: " + bb_max + "\n");
        writer.write("Total matches won: " + bb_output + "\n");
        writer.write("Teams who put three pointers: " + bb_result + "\n");
        writer.close();
        System.out.println("\nWBB OUTPUT3.txt file successfully written.");
      } catch (IOException e) {
        System.out.println("An error has occurred.");
        e.printStackTrace();
      }
  }

 
  }