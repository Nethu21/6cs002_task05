package W_Basketball;
/******************************************************************************
 * This class stores the details of a Premiership rugby bbclub_names including the 
 * performance measures that determine their placements in the league. The class 
 * implements the Comparable interface, which determines how clubs will be 
 * sorted.     
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class WBBClub implements Comparable<WBBClub> {
  private int placements;
  private String bbclub_names;
  private int matches_played;
  private int bb_matches_won;
  private int bb_matches_drawn;
  private int bb_matches_lost;
  private int pointscored;
  private int disqualifedperMember;
  private int total_won;
  private int total_lost;
  private int halfcourt;
  private int threePointersMade;
  private int freeThrowsMade;
  private int foulsMade;


  public WBBClub(int placements, String bbclub_names, int matches_played, int bb_matches_won, 
		  int bb_matches_drawn,int bb_matches_lost, int pointscored, int disqualifedperMember, 
		  int total_won, int total_lost,int halfcourt,int threePointersMade, int freeThrowsMade, 
		  int foulsMade) {
    this.placements = placements;
    this.bbclub_names = bbclub_names;
    this.matches_played = matches_played;
    this.bb_matches_won = bb_matches_won;
    this.bb_matches_drawn = bb_matches_drawn;
    this.bb_matches_lost = bb_matches_lost;
    this.pointscored = pointscored;
    this.disqualifedperMember = disqualifedperMember;
    this.total_won = total_won;
    this.total_lost = total_lost;
    this.halfcourt = halfcourt;
    this.threePointersMade = threePointersMade;
    this.freeThrowsMade = freeThrowsMade;
    this.foulsMade = foulsMade;
   
  }

  public String toString() {
	  return String.format("%-3d%-20s%10d%10d%10d%10d", placements, bbclub_names, pointscored, 
			  total_won, total_lost,bb_matches_drawn);
  }
  

  public int getPosition() {
    return placements;
  }

  public void setPosition(int position) {
    this.placements = position;
  }

  public String getClub() {
    return bbclub_names;
  }

  public void setClub(String club) {
    this.bbclub_names = club;
  }

  public int getPlayed() {
    return matches_played;
  }

  public void setPlayed(int played) {
    this.matches_played = played;
  }

  public int getWon() {
    return bb_matches_won;
  }

  public void setWon(int won) {
    this.bb_matches_won = won;
  }

  public int getDrawn() {
    return bb_matches_drawn;
  }

  public void setDrawn(int drawn) {
    this.bb_matches_drawn = drawn;
  }

  public int getLost() {
    return bb_matches_lost;
  }

  public void setLost(int lost) {
    this.bb_matches_lost = lost;
  }

  public int gethalfcourt() {
	    return halfcourt;
	  }

  public void sethalfcourt(int halfcourt) {
	    this.halfcourt = halfcourt;
	  }

  public int getpointscored() {
	    return pointscored;
  }

  public void setpointscored(int pointscored) {
	    this.pointscored = pointscored;
  }

public int getdisqualifedperMember() {
    return disqualifedperMember;
  }

  public void setdisqualifedperMember(int disqualifedperMember) {
    this.disqualifedperMember = disqualifedperMember;
  }

  public int total_won() {
    return total_won;
  }

  public void settotalwon(int totalwon) {
    this.total_won = totalwon;
  }

  public int gettotallost() {
    return total_lost;
  }

  public void settotallost(int totallost) {
    this.total_lost = totallost;
  }

  public int getthreePointersMade() {
    return threePointersMade;
  }

  public void setthreePointersMade(int threePointersMade) {
    this.threePointersMade = threePointersMade;
  }

  public int getfreeThrowsMade() {
    return freeThrowsMade;
  }

  public void setfreeThrowsMade(int freeThrowsMade) {
    this.freeThrowsMade = freeThrowsMade;
  }

  public int getfoulsMade() {
    return foulsMade;
  }

  public void setfoulsMade(int foulsMade) {
    this.foulsMade = foulsMade;
  }

  public int compareTo(WBBClub c) {
    return ((Integer) disqualifedperMember).compareTo(c.disqualifedperMember);
  }
}
