package W_Basketball;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class W_Basketball_02 {
  public static void main(String[] args) {
	  List<WBBClub> table = Arrays.asList(
		        new WBBClub(1, "Southside Lynx", 11, 10, 0, 1, 493,0,450,43,6, 24,10,5),
		        new WBBClub(2, "Perth Flyers", 11, 6, 2, 3, 481,2,270,129,0,9,5,4),
		        new WBBClub(3, "Sydney Spirit", 11, 5, 1, 5, 480,2,225,215,0,0,6,3),
		        new WBBClub(4, "Flames Bendigo", 11, 4, 1, 6, 478,2,180,258,0,0,2,1),
		        new WBBClub(5, "Townsville Flames", 11, 7,2,2,485,0,315,86,3,3,3,2),
		        new WBBClub(6, "Coburg Kougars", 11, 8, 1, 2, 488,0,360,86,3,6,4,2),
		        new WBBClub(7, "St. Kilda", 11, 5, 1, 5, 480,2,225,215,0,0,2,1),
		        new WBBClub(8, "Melbourne Sharks", 11, 8, 1, 2, 488,0,360,86,3,9,5,4),
		        new WBBClub(9, "Sutherland Tigers", 11, 5, 2, 24, 481,0,225,172,0,0,2,1),
		        new WBBClub(10, "East Bearcats", 11, 9, 1, 1, 488,2,405,43,3,0,2,1),
		        new WBBClub(11, "City Comets", 11, 6, 1, 4, 484, 0,270,172,0,3,1,1),
		        new WBBClub(12, "SLSM Hawks", 11, 11, 0, 0, 495,0,495,0,9,44,14,7));
		       
	System.out.println("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
    table.stream().forEach(b -> System.out.println(b));
    System.out.println();
    table.parallelStream().forEach(System.out::println);
    
    try {
		FileWriter writer = new FileWriter("WBB_OUTPUT2.txt");
		writer.write("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
		writer.write("   ---------                ------    ---------  ---------- ------\n");
		table.stream().forEach(x -> {
		try {
			writer.write(x + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		});
		
		writer.write("\n Parallel Stream \n");
		writer.write("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
		writer.write("   ---------                ------    ---------  ---------- ------\n");
		table.parallelStream().forEach(x -> {
			try {
				writer.write(x.toString() + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		writer.close();
		System.out.println("\nWBB OUTPUT2.txt file successfully written.");
    } catch (IOException e) {
    	System.out.println("An error has occurred.");
    	e.printStackTrace();
    }
}


}

